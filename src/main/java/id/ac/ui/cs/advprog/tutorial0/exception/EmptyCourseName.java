package id.ac.ui.cs.advprog.tutorial0.exception;

public class EmptyCourseName extends RuntimeException{
    public EmptyCourseName(){
        super("Course Name cannot be empty");
    }
}
