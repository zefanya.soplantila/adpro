package id.ac.ui.cs.advprog.tutorial0.service;

import id.ac.ui.cs.advprog.tutorial0.exception.EmptyCourseName;
import id.ac.ui.cs.advprog.tutorial0.model.Student;
import id.ac.ui.cs.advprog.tutorial0.repository.CourseRepository;
import id.ac.ui.cs.advprog.tutorial0.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial0.model.Course;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class CourseServiceImpl implements CourseService{

    @Autowired
    private CourseRepository courseRepository;

    public Course create(Course course){
        generateCourseId(course);
        validateEmptyName(course);
        courseRepository.create(course);
        return course;
    }

    public void validateEmptyName(Course course){
        if(course.getCourseName().equals(""))
            throw new EmptyCourseName();
    }

    private void generateCourseId(Course course) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char letter: course.getCourseName().toCharArray()) {
            stringBuilder.append(String.valueOf((int)letter));
        }
        String courseId = stringBuilder.toString();
        course.setCourseId(courseId);
    }

    @Override
    public List<Course> findAll() {
        Iterator<Course> courseIterator = courseRepository.findAll();
        List<Course> allCourses = new ArrayList<>();
        courseIterator.forEachRemaining(allCourses::add);
        return allCourses;
    }
}
